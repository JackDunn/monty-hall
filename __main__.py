print("The Monty Hall Problem")

import random, copy, json

class MontyHallGame:
    def __init__(self):
        # initialise everything
        self.doors = [0, 0, 0]
        self.stage = 0 # 0: Nothing selected, 1: first door selected, 2: final door 
        self.selection = None
        self.firstSelection = None
        self.finalSelection = None
        self.prize = -1 # not none, as that can be interpreted as 0

        # set a random door to have the Million Pound Cash Prize!
        self.doors[random.randint(0, len(self.doors)-1)] = 1
    
    def selectFirstDoor(self, selection, override = False):
        if self.stage == 0 or override:
            # progress stage
            self.stage = 1
            # save the selection for later
            self.selection = selection
            self.firstSelection = selection

            doorToOpen = selection
            # keep selecting a random door until it is not the cash prize or the one selected
            while doorToOpen == selection or self.doors[doorToOpen] == 1:
                doorToOpen = random.randint(0, len(self.doors)-1)
            # the end result will be a door with a goat
            self.openedDoor = doorToOpen
        return self.openedDoor

    def selectFinalDoor(self, selection, override = False):
        if self.stage == 1 or override:
            # progress stage
            self.stage = 2
            # save selection for later
            self.selection = selection
            self.finalSelection = selection
            # save final prize
            self.prize = self.doors[selection]
        return self.doors[selection]

class SimulateOneGame:
    def __init__(self):
        self.Game = MontyHallGame()
        # make copies of one game, so that the doors are exactly the same
        self.stickGame = copy.deepcopy(self.Game)
        self.twistGame = copy.deepcopy(self.Game)
        self.randomGame = copy.deepcopy(self.Game)

    def makeFirstSelection(self, selection=None):
        # fix an unspecified selection
        if selection == None:
            selection = random.randint(0,len(self.Game.doors)-1)
        # save for later
        self.originalSelection = selection

        # select the first door
        self.stickGame.selectFirstDoor(selection)
        self.twistGame.selectFirstDoor(selection)
        self.randomGame.selectFirstDoor(selection)

        return selection

    def _stick(self, game):
        return game.selectFinalDoor(self.originalSelection)

    def _twist(self, game):
        availableOptions = [0, 1, 2]
        availableOptions.remove(self.originalSelection)
        availableOptions.remove(game.openedDoor)
        # the last available option should be the one other unopened door
        return game.selectFinalDoor(availableOptions[0])

    def makeFinalSelection(self):
        self.stickResult = self._stick(self.stickGame)
        self.twistResult = self._twist(self.twistGame)
        
        # random game
        if random.randint(0,1) == 0:
            self.randomResult = self._stick(self.randomGame)
        else:
            self.randomResult = self._twist(self.twistGame)

        return [self.stickResult, self.twistResult, self.randomResult]

# Main program
class __main__:
    def __init__(self):
        while True:
            # display menu to show options
            print("""
    -= MENU =-
1) Run new simulation 
2) Continue past simulation 
Please choose 1 or 2.      
""")
            choice = input("> ")
            # catch the user entering an incorrect choice
            while not choice in ["1","2"]:
                print("Invalid choice! Please choose 1 or 2.")
                choice = input("> ")
            if choice == "1":
                # run a new simulation (without a file)
                self.runSimulation()
            elif choice == "2":
                # enter a file path, and ensure it's a valid file
                valid = False
                while not valid:
                    filepath = input("Enter the file path of the existing simulation: ")
                    if filepath == "":
                        print("Please don't leave it blank!")
                    try:
                        # Open file (check it exists)...
                        with open(filepath) as f:
                            jsonData = f.read()
                        # ...and load the json data (check it's valid json)
                        data = json.loads(jsonData)
                        # if these work, it's valid.
                        valid = True
                    except:
                        # if any of these error, it's invalid, so output an error.
                        print("Please ensure the file exists, and that it is a valid simulation file.")
                self.runSimulation(filepath)
    
    def runSimulation(self, filepath=None):
        # prepare the data variable
        data = {
            "wins": {
                "stick": 0,
                "twist": 0,
                "random": 0
            },
            "games": []
        }
        # if a filepath was provided, load in the data
        if filepath != None:
            with open(filepath) as f:
                jsonData = f.read()
            data = json.loads(jsonData)
            saveResult = True
        else:
            print("Save results? (if yes, type file path. Else, leave blank)")
            filepath = input(">")
            saveResult = (filepath != "")
        
        print("\n\n\n\n")
        print("Press ctrl-C to break loop.")
        print("0 simulations carried out",end="")
        # print out the wins from each individual type of game
        for gameType in ["Stick","Twist","Random"]:
            print(" // "+gameType+" wins: 0",end="")
        print("\r", end="")
        try:
            while True:
                # prepare game data and game simulation
                gameData = {}
                oneGame = SimulateOneGame()
                # make the selections
                gameData["originalSelection"] = oneGame.makeFirstSelection()
                result = oneGame.makeFinalSelection()
                # save the results
                gameData["result"] = result
                data["wins"]["stick"] += oneGame.stickResult
                data["wins"]["twist"] += oneGame.twistResult
                data["wins"]["random"] += oneGame.randomResult
                gameData["doors"] = oneGame.Game.doors
                # update the output
                print(str(len(data["games"]))+" simulations carried out", end="")
                for gameType in ["Stick","Twist","Random"]:
                    print(" // "+gameType+" wins: "+str(data["wins"][gameType.lower()]),end="")
                print("\r", end="")
                data["games"].append(gameData)
                # save the simulations when necessary
                if saveResult and len(data["games"]) % 1000 == 999:
                    print("Game count checkpoint reached, saving...    ",end="\r")
                    with open(filepath,'w') as f:
                        f.write(json.dumps(data))
        except KeyboardInterrupt:
            # received kill key-combo, so finish things up
            # Final output
            print("\r"+ str(len(data["games"]))+" simulations carried out", end="")
            for gameType in ["Stick","Twist","Random"]:
                print(" // "+gameType+" wins: "+str(data["wins"][gameType.lower()]),end="")
            # save the data
            print("")
            if saveResult and len(data["games"]) % 1000 == 999:
                print("Saving...",end="\r")
                with open(filepath,'w') as f:
                    f.write(json.dumps(data))
                print("Saved to "+filepath)
            # simulation section ends, return to menu

# if this is the main application (i.e. not imported) run the main program.
if __name__ == "__main__":
    __main__()
